import random
import numpy as np


class Assignment1:
    def create_list(self, num, start_num, end_num):
        arr_list = []
        temp = random.randint(int(start_num), int(end_num))
        for x in range(int(num)):
            while temp in arr_list:
                temp = random.randint(start_num, end_num)
            arr_list.append(temp)
        print('List of Random Numbers : ', arr_list)
        return arr_list

    def sort_list(self, arr_list):
        arr_list.sort()
        print('Sorted Numbers in Ascending Order : ', arr_list)
        return arr_list

    def verify_even_numbers(self, sort_arr_list):
        # num = 0
        eve_num_list = []
        for num in sort_arr_list:
            if num % 2 == 0:
                # print(num, end=" ")    # end is used to give space and print next number in same line
                eve_num_list.append(num)
        print('Even Numbers From List : ', eve_num_list)
        return eve_num_list

    def even_num_square(self, num, start_num, end_num):
        array_list = self.create_list(num, start_num, end_num)
        sort_arr_list = self.sort_list(array_list)
        even_num_list = self.verify_even_numbers(sort_arr_list)
        squ_list = np.square(even_num_list)
        print('Square of Even Numbers : ', squ_list)


