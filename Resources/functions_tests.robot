*** Settings ***
Library  DebugLibrary
Resource  ../Resources/variables.robot
Library  ../Libraries/Assignment1.py
Library  ../Libraries/Assignment2.py
Library  BuiltIn

*** Keywords ***
Sort Numbers And Print Square of Even Numbers
    [Documentation]    This Keyword will create a list of random numbers, sort it in ascending order.
    ...                It will find even numbers from the list and print the square of that even numbers.
    ${SIZE_OF_LIST}   Convert To Integer  ${SIZE_OF_LIST}
    ${STARTING_FROM}  Convert To Integer  ${STARTING_FROM}
    ${LAST_NUMBER}    Convert To Integer  ${LAST_NUMBER}
    even num square    ${SIZE_OF_LIST}   ${STARTING_FROM}  ${LAST_NUMBER}

Sort Cities And Print
    [Documentation]   This keyword will take the array of cities, sort it in ascending order and print it.
    cities_ops

Verify Search Product
    [Documentation]   This Keyword will launch the amazon web application. It will search the Camera from search box.
    ...               After that it will Sort based on price from High to low and verify the searched first product
    ...               is of 'Canon' brand or not.
    Launch Amazon Application
    Search Camera In Search Text Box
    Sort Search Result By price
    Verify First Product From Search List

Open New Browser
    [Documentation]   This Keyword will launch the browser and maximize the browser window
    Open Browser  about:blank   ${BROWSER}
    Set Selenium Speed   150 ms
    Maximize Browser Window

Launch Amazon Application
    [Documentation]   This keyword will navigate to Amazon web application
    Go To  ${Amazon_URL}

Search Camera In Search Text Box
    [Documentation]   This keyword will search 'Camera' in search text box
    Wait Until Element Is Visible    ${SEARCH_TEXT_BOX}   timeout=5s
    Input Text   ${SEARCH_TEXT_BOX}   ${SEARCH_TEXT}
    Wait Until Element Is Visible   ${SEARCH_ICON}   timeout=5s
    Click Element   ${SEARCH_ICON}

Sort Search Result By price
    [Documentation]   This keyword will sort the searched product based on price from high to low
    Wait Until Element Is Visible   ${SORT_BY_DROPDOWN}  timeout=5s
    Click Element   ${SORT_BY_DROPDOWN}
    Wait Until Element Is Visible   ${SORT_HIGH_TO_LOW_OPTION}  timeout=5s
    Click Element  ${SORT_HIGH_TO_LOW_OPTION}

Verify First Product From Search List
    [Documentation]   This keyword will open first searched product and verify that first product brand is 'Canon or not
    Wait Until Element Is Visible   ${SEARCH_CAMERA_LINKS}  timeout=20s
    ${all_links_count}   Get Element Count   ${SEARCH_CAMERA_LINKS}
    log  ${all_links_count}
    Wait Until Element Is Visible   ${FIRST_SEARCH_PRODUCT}   timeout=5s
    Click Element  ${FIRST_SEARCH_PRODUCT}
    Wait Until Element Is Visible   ${BRAND_NAME_LINK}   timeout=5s
    ${brand_name}   Get Text   ${BRAND_NAME_LINK}
    Log   ${brand_name}
    ${status}  ${value}  Run Keyword And Ignore Error  Should Contain  ${brand_name}  ${BRAND_NAME_VALUE}
    IF  '${status} ' == 'PASS'
        Log  Brand Name is Canon
    ELSE
        Log  Brand Name is not Canon
    END

Closing Browser
    [Documentation]   This keyword will close the browser
    Run Keyword and Ignore Error   Close Browser