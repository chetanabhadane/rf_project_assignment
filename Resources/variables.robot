*** Settings ***
Documentation  Obeject variables repository

*** Variables ***
#Assignment1
${SIZE_OF_LIST}   5
${STARTING_FROM}   1
${LAST_NUMBER}   10

#Assignment3
${BROWSER}   chrome
${Amazon_URL}   https://www.amazon.com/
${SEARCH_TEXT_BOX}   xpath://input[@id="twotabsearchtextbox"]
${SEARCH_TEXT}   Camera
${SEARCH_ICON}   xpath://input[@id="nav-search-submit-button"]
${SORT_BY_DROPDOWN}  xpath://span[@data-action="a-dropdown-button"]
${SORT_HIGH_TO_LOW_OPTION}  xpath://a[text() = "Price: High to Low"]
${SEARCH_CAMERA_LINKS}  xpath://h2/a[contains(@class,"a-link-normal s-link-style a-text-normal")]
${FIRST_SEARCH_PRODUCT}    xpath:(//a[@class="a-link-normal s-link-style a-text-normal"])[1]
${BRAND_NAME_LINK}   xpath://div[@id="bylineInfo_feature_div"]/div/a
${BRAND_NAME_VALUE}   Canon


