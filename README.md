Installation Guidelines

1. Install Python (3.7 / 3.8)
2. Install Pycharm Community Edition
3. Install Robot Framework Plugin
4. Download browser webdriver as per the browser version. Place it in 'Python-> Scripts' folder
5. Install Libraries (Pip, selenium Library, etc..) 
6. Create a folder structure like libraries, resources , results, tests
7. create a python file to write methods/functions in libraries folder
8. create variables file in resources folder to maintain the variables
9. write keywords in functions file in resources folder
10. Write test cases in test suite file in 'Tests' folder
11. Execute all the test cases present in 'testsuite' file using command - robot -d Results Tests/Assignments_TestSuite.robot
12. Execute all the test cases present in 'testsuite' file based on tags using command - robot -d Results --include [tag name] Tests/Assignments_TestSuite.robot - here tag name is 'unit', 'sanity', 'smoke', etc...
13. Execute test cases excluding some test cases based on tags - robot -d Results --exclude smoke Tests/Assignments_TestSuite.robot

14. 

