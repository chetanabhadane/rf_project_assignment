*** Settings ***
Library   SeleniumLibrary
Resource  ../Libraries/Assignment1.py
Resource  ../Libraries/Assignment2.py
Resource  ../Resources/functions_tests.robot
Library   OperatingSystem


*** Test Cases ***
Assignment1 Sorting Of Numbers And Print Square Of Even Numbers
    [Tags]   Unit
    [Documentation]    This Test case will sort the numbers from the list, finds the even numbers and print square of even numbers
    Sort Numbers And Print Square of Even Numbers

Assignment2 Sorting of Cities From List
    [Tags]   Smoke
    [Documentation]    This Test case will sort the cities from the list and print the sorted lists
    Sort Cities And Print

Assignment3 Sort The Searched Items
    [Tags]   Sanity
    [Documentation]   This test Case will sort the search items from price low to high and will verify the searched product is 'canon' or not
    Open New Browser
    Verify Search Product
    Closing Browser
